<?php 
if(isset($_POST['email'])){
    // if url is set in form - but url is also empty, then it is probably not a spam-bot
    // note: the url field is set to "display:none" - only a bot will fill it in
    if(isset($_POST['url']) && $_POST['url'] == ''){
        $to = "thailand@techgrind.asia"; // this is your Email address
        $from = $_POST['email']; // this is the sender's Email address
        $name = $_POST['name'];
        $subject = "[Contact Form: TGTH Incubator]";
        $subject2 = "Copy of your form submission";
        $message = "[ " . $name . " ] with the email address: [ " . $from . " ] wrote the following:" . "\n\n" . $_POST['message'];
        $message2 = "Here is a copy of your message " . $name . "\n\n" . $_POST['message'];

        $headers = "From:" . "postmaster@techgrind.asia";
        $headers2 = "From:" . $to;
        mail($to,$subject,$message,$headers);
        mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
        //echo "Mail Sent. Thank you " . $name . ", we will contact you shortly.";
        header('location: index.html');
        // You can also use header('Location: thank_you.php'); to redirect to another page.
        // You cannot use header and echo together. It's one or the other.
        } else { header('Location: http://www.google.com'); }
    } else { echo "your email is not set"; }
?>